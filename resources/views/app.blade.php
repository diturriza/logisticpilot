<!DOCTYPE html>
<html>
<head>
	<title>RecordDEMO</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
	<body>
		<div class="container">
			@yield('content')
		</div>
		<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		@yield('customScript')
	</body>
</html>