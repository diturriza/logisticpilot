@extends('layout')

@section('content')
<style type="text/css">
.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
</style>
<form class="form-signin"  method="POST" action={{ action('Auth\AuthController@postRegister') }}>
    <h2 class="form-signin-heading">Please sign up</h2>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <label for="name" class="sr-only">Name</label>
    <input type="text" name="name" class="form-control" placeholder="Email address" required="" autofocus="">
    <label for="email" class="sr-only">Email address</label>
    <input type="email" name="email" class="form-control" placeholder="Email address" required="" autofocus="">
    <label for="password" class="sr-only">Password</label>
    <input type="password" name="password" class="form-control" placeholder="Password" required="">
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
 </form>
 @stop