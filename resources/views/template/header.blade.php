  <div class="top-bar">
    <div class="wrapper">
      <div class="row">
        <div class="col-xs-12 col-sm-2 col-md-2">
          <div class="logo"><a href="#">{!! Html::image( asset('images/logo.png')) !!} </a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3">
        	<h2 class="tagline">This is a nice description about logo</h2>
        </div>
        <div class="col-xs-12 col-sm-7 col-md-7">
          <div class="login pull-right">
            <div class="dropdown">
              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"> <i class="fa fa-power-off"></i><span>Logout</span> <span class="caret"></span> </button>
              <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
              </ul>
            </div>
          </div>
          <div class="user-menu pull-right">
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <h4 class="hidden-xs">Welcome <strong>Bryan Cole</strong> </h4>
              <figure>  {!! Html::image( asset('images/avtar.png')) !!} </figure>
              <span class="caret"></span></button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#"><i class="fa fa-music"></i>activity log</a></li>
                <li><a href="#"><i class="fa fa-laptop"></i>news feed</a></li>
                <li><a href="#"><i class="fa fa-cogs"></i>settings</a></li>
                <li><a href="#"><i class="fa fa-sign-out"></i>logout</a></li>
              </ul>
            </div>
          </div>
          <div class="support-team pull-right">
            <div class="dropdown">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Your Support Team <span class="caret"></span></button>
              <ul class="dropdown-menu" role="menu">
              	<li><a id="btn-recorder" href="#"><span class="glyphicon glyphicon-record btn-icon" aria-hidden="true"></span>  Recorder </a></li>
              	<li><a id="btn-messages" href="{{ action('IssueController@index') }}"><span class="glyphicon glyphicon-envelope btn-icon" aria-hidden="true"></span>  Messages </a></li>
  		<li role="separator" class="divider"></li>
                <li><a href="#">Call <span>875-222-2323</span></a></li>
                <li><a href="#">Live Chat <span class="status active"></span></a></li>
                <li><a href="#">Screen Cast</a></li>
              </ul>
            </div>
          </div>
          <ul class="theme_list">
          	<li>Color Theme:</li>
              <li><a href="javascript:void(0);" id="default_theme" data-toggle="tooltip" data-placement="bottom" title="Default">Default</a></li>
              <li><a href="javascript:void(0);" id="green_theme" data-toggle="tooltip" data-placement="bottom" title="Green">Green</a></li>
              <li><a href="javascript:void(0);" id="orange_theme" data-toggle="tooltip" data-placement="bottom" title="Orange">Orange</a></li>
              <li><a href="javascript:void(0);" id="red_theme" data-toggle="tooltip" data-placement="bottom" title="Red">Red</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="row clearfix">
    <div class="col-xs-12 col-lg-12">
      <nav role="navigation" class="navbar navbar-default navbar-fixed-top main-menu">

        <!-- /.navbar-collapse -->
      </nav>
    </div>
  </div>