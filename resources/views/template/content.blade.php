@extends('layout')

@section('title')
Template Demo
@endsection

@section('content')
          <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="row breadcrumb-cont">
              <div class="col-lg-6 col-md-6W col-xs-12 filter-block">

              </div>
              <div class="col-lg-6 col-md-6W col-xs-12">
                <ol class="breadcrumb">
                  <li><a href="#">Home <i class="fa fa-angle-right"></i></a></li>
                  <li><a href="#">Library <i class="fa fa-angle-right"></i></a></li>
                  <li class="active">Data</li>
                </ol>
              </div>
            </div>

            <div class="contract_table">
                <div class="reg_top">
                    <h2>Registration Process</h2>
                    <ul class="reg_steps">
                        <li class="step_1 active"><span>1</span> Enter Company Details</li>
                        <li class="step_2"><span>2</span> Enter Bill to Details</li>
                        <li class="step_3"><span>3</span> Select Your Carriers</li>
                        <li class="step_4"><span>4</span> Setup Carrier Contract</li>
                        <li class="step_5"><span>5</span> Test My Rates</li>
                        <li class="step_6"><span>6</span> Setup Markup/Upcharge</li>
                    </ul>
                </div>
                <div class="reg_bot">
                    <h3>Fill Company Details</h3>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Your Company Name*</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Company Address Line 1</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Company Address Line 2</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Country*</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>City*</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>State*</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Postal*</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Contact Name</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Contact Email</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Contact Phone</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Fax</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <label>Upload Company Logo</label>
                            <div class="file_upload">
                                <button type="button" class="btn btn-primary">Upload Logo</button>
                                <p>( Instruction about file size and image width and height )</p>
                            </div>
                            <label>Banner Message</label>
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="btn-cont">
                        <button  type="button" class="btn btn-primary">Continue Later</button>
                        <button  type="button" class="btn btn-primary">Next</button>
                    </div>
                </div>
          	</div>
          </div>
@endsection