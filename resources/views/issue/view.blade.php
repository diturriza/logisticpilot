@extends('layout')

@section('content')
<style type="text/css" media="screen">
	.row{
		margin-bottom: 5px;
	}
</style>
<div class="contract_table">
    <h2>Issue # {{ $issue->id }}</h2>
	   	<div class="col-md-6">
	   		@if ($issue->mediaType == 'image')
	    		<img class="img-responsive" src="{{ asset($issue->mediaUrl) }}" alt="">
	    	@else
	    		<video src="{{ asset($issue->mediaUrl) }}" autobuffer autoloop loop controls poster="/images/video.png"></video>
	    	@endif
	    </div>
	    <div class="col-md-6">
	    	<div class="row">
	    		@if(Auth::user()->isAdmin )
	    			<h3>Write by: {{ $issue->user->name}}:</h3>
	    		@else
	    			<h3>You write:</h3>
	    		@endif
	    		<p class="text-justify">{{ $issue->issueTxt }}</p>
	    	</div>
	    	<div class="row">
			    @if (Auth::user()->isAdmin)
			    	@if ($issue->response)
			      		<h3>Response by {{ $issue->admin->name }}</h3>
			      		<textarea name="issue" id="issue" class="form-control" rows="3" disabled="">{{ $issue->response }}</textarea>
			      	@else
			      		<h3>{{ Auth::user()->name }}, you can solve this issue</h3>
						{!! Form::model($issue, [
						    'method' => 'PATCH',
						    'route' => ['issues.update', $issue->id]
						]) !!}
							{!! Form::hidden('admin_id',Auth::user()->id, ['class' => 'form-control']) !!}
    						{!! Form::textarea('response', null, ['class' => 'form-control']) !!}
							{!! Form::submit('Update Issue', ['class' => 'btn btn-primary']) !!}
						{!! Form::close() !!}
			      	@endif
			    @else
			    	@if( $issue->response )
			      		<h3>Hi {{ Auth::user()->name }}, {{ $issue->admin->name }} response this</h3>
			      		<textarea name="issue" id="issue" class="form-control" rows="3" disabled=""></textarea>
			      	@else
			      		<h3>Hi {{ Auth::user()->name }}, :( We haven't found the solution</h3>
			      	@endif
			    @endif
	    	</div>
	    </div>
</div>

@endsection
