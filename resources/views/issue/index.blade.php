@extends('layout')

@section('content')
<style>
.table{
  border-radius:5px;
}
</style>
<div class="contract_table">
  @if(\Auth::check() && \Auth::user()->isAdmin)
    <h2>Issues</h2>
  @else
    <h2>Hi {{ (\Auth::check())?\Auth::user()->name:"User not logged" }}, your issues</h2>
  @endif

  <div class="issues-list">
    <div class="row">
      <ul class="nav nav-pills" role="tablist">
        <li role="presentation" class="active"><a href="all" >All <span class="badge">{{ $data['count_issues'] }}</span></a></li>
        <li role="presentation"><a href="solved" >Solved <span class="badge">{{ $data['count_solved'] }}</span></a></li>
        <li role="presentation"><a href="nosolved">Not Solved <span class="badge">{{ $data['count_nosolved'] }}</span></a></li>
      </ul>
    </div>
    <div class="row">
      <table class="table table-hover">
        <thead>
          <tr>
            <th># Issue</th>
            <th>Name of User</th>
            <th>Resume Issue</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody class="text-center">
        @foreach ($data['issues'] as $issue)
          <tr class="{{($issue->response)?'success':'danger'}}">
            <td>{{ $issue->id }}</td>
            <td>{{ $issue->user->name }}</td>
            <td class="text-justify" width="70%">{{ $issue->issueTxt }}</td>
            <td><form class="form-signin" method="GET" action="{{ action('IssueController@show', ['id' => $issue->id]) }}"><button type="submit" class="btn btn-primary" >View</button></form></td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
    <div class="row text-center">
      {!! $data['issues']->render() !!}
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
    // $(window).on('hashchange',function(){
    //   page = window.location.hash.replapaginationce('#','');
    //   // console.log('Hash change: '+page);
    //   getIssues(page);
    // });

    $(document).on('click','.pagination a', function(e){
      e.preventDefault();
      var page = $(this).attr('href').split('page=')[1];
      // console.log('Click a : '+page);
      getIssues(page);
    });

    $(document).on('click','.nav-pills a', function(e){
      e.preventDefault();
      var filter = $(this).attr('href');
      console.log('Click a nav-pills: '+filter);
      getIssuesFiltered(filter);
      location.hash = filter;
    });

    function getIssues(page){
      var search = window.location.hash;
      console.log(search);
      if(search=='solved'){
        search = '?filter=solved&';
      }else{
        if(search == 'nosolved'){
          search = '?filter=nosolved&';
        }else{
          search = '?';
        }
      }
      $.ajax({
        type:'GET',
        url: window.location.pathname+'/issue' + search + 'page=' + page
      }).done(function(data){
        $('.issues-list').html(data);
      });
    }

    function getIssuesFiltered(filter){
      $.ajax({
        type:'GET',
        url: window.location.pathname + '/issue?filter=' + filter
      }).done(function(data){
        $('.issues-list').html(data);
        $('.active').removeClass('active');
        $('a[href="'+filter+'"]').parents('li').addClass('active');
      });
    }

</script>
@endsection