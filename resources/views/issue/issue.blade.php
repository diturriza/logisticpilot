
    <div class="row">
      <ul class="nav nav-pills" role="tablist">
        <li role="presentation" class="active"><a href="all" >All <span class="badge">{{ $data['count_issues'] }}</span></a></li>
        <li role="presentation"><a href="solved" >Solved <span class="badge">{{ $data['count_solved'] }}</span></a></li>
        <li role="presentation"><a href="nosolved">Not Solved <span class="badge">{{ $data['count_nosolved'] }}</span></a></li>
      </ul>
    </div>
    <div class="row">
      <table class="table table-hover">
        <thead>
          <tr>
            <th># Issue</th>
            <th>Name of User</th>
            <th>Resume Issue</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody class="text-center">
        @foreach ($data['issues'] as $issue)
          <tr class="{{($issue->response)?'success':'danger'}}">
            <td>{{ $issue->id }}</td>
            <td>{{ $issue->user->name }}</td>
            <td class="text-justify" width="70%">{{ $issue->issueTxt }}</td>
            <td><form class="form-signin" method="GET" action="{{ action('IssueController@show', ['id' => $issue->id]) }}"><button type="submit" class="btn btn-primary" >View</button></form></td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
    <div class="row text-center">
      {!! $data['issues']->render() !!}
    </div>