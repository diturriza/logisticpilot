<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <title>Logistics Pilot</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700,600' rel='stylesheet' type='text/css'>
    {!! Html::style( asset('css/bootstrap.min.css')) !!}
    {!! Html::style( asset('css/flexslider.css')) !!}
    {!! Html::style( asset('css/jquery-ui.css')) !!}
    {!! Html::style( asset('css/ion.rangeSlider.css')) !!}
    {!! Html::style( asset('css/ion.rangeSlider.skinHTML5.css')) !!}
    {!! Html::style( asset('css/jquery.multiselect.css')) !!}
    {!! Html::style( asset('css/footable.core.css')) !!}
    {!! Html::style( asset('css/easy-responsive-tabs.css')) !!}
    {!! Html::style( asset('css/bootstrap-timepicker.min.css')) !!}
    {!! Html::style( asset('css/screen.css')) !!}
    {!! Html::style( asset('css/font-awesome.min.css')) !!}
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>
  <body>
  <meta name="_token" content="{!! csrf_token() !!}"/>
  <div class="top-bar">
    <div class="wrapper">
      <div class="row">
        <div class="col-xs-12 col-sm-2 col-md-2">
          <div class="logo"><a href="#"><img src="images/logo.png"></a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3">
        	<h2 class="tagline">This is a nice description about logo</h2>
        </div>
        <div class="col-xs-12 col-sm-7 col-md-7">
          <div class="login pull-right">
            <div class="dropdown">
              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true"> <i class="fa fa-power-off"></i><span>Logout</span> <span class="caret"></span> </button>
              <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
              </ul>
            </div>
          </div>
          <div class="user-menu pull-right">
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <h4 class="hidden-xs">Welcome <strong>Bryan Cole</strong> </h4>
              <figure><img src="images/avtar.png" alt=""> </figure>
              <span class="caret"></span></button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#"><i class="fa fa-music"></i>activity log</a></li>
                <li><a href="#"><i class="fa fa-laptop"></i>news feed</a></li>
                <li><a href="#"><i class="fa fa-cogs"></i>settings</a></li>
                <li><a href="#"><i class="fa fa-sign-out"></i>logout</a></li>
              </ul>
            </div>
          </div>
          <div class="support-team pull-right">
            <div class="dropdown">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Your Support Team <span class="caret"></span></button>
              <ul class="dropdown-menu" role="menu">
              	<li><a id="btn-recorder" href="#"><span class="glyphicon glyphicon-record btn-icon" aria-hidden="true"></span>  Recorder </a></li>
              	<li><a id="btn-messages" href="{{ action('IssueController@index') }}"><span class="glyphicon glyphicon-envelope btn-icon" aria-hidden="true"></span>  Messages </a></li>
  		<li role="separator" class="divider"></li>
                <li><a href="#">Call <span>875-222-2323</span></a></li>
                <li><a href="#">Live Chat <span class="status active"></span></a></li>
                <li><a href="#">Screen Cast</a></li>
              </ul>
            </div>
          </div>
          <ul class="theme_list">
          	<li>Color Theme:</li>
              <li><a href="javascript:void(0);" id="default_theme" data-toggle="tooltip" data-placement="bottom" title="Default">Default</a></li>
              <li><a href="javascript:void(0);" id="green_theme" data-toggle="tooltip" data-placement="bottom" title="Green">Green</a></li>
              <li><a href="javascript:void(0);" id="orange_theme" data-toggle="tooltip" data-placement="bottom" title="Orange">Orange</a></li>
              <li><a href="javascript:void(0);" id="red_theme" data-toggle="tooltip" data-placement="bottom" title="Red">Red</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="row clearfix">
    <div class="col-xs-12 col-lg-12">
      <nav role="navigation" class="navbar navbar-default navbar-fixed-top main-menu">

        <!-- /.navbar-collapse -->
      </nav>
    </div>
  </div>
  <div class="body-container contract-page" id="page-wrapper">
    <!-- content section-->
    <section class="content-wrapper full-width">
      <div class="content-subcontainer">
        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="row breadcrumb-cont">
              <div class="col-lg-6 col-md-6W col-xs-12 filter-block">

              </div>
              <div class="col-lg-6 col-md-6W col-xs-12">
                <ol class="breadcrumb">
                  <li><a href="#">Home <i class="fa fa-angle-right"></i></a></li>
                  <li><a href="#">Library <i class="fa fa-angle-right"></i></a></li>
                  <li class="active">Data</li>
                </ol>
              </div>
            </div>

            <div class="contract_table">
                <div class="reg_top">
                    <h2>Registration Process</h2>
                    <ul class="reg_steps">
                        <li class="step_1 active"><span>1</span> Enter Company Details</li>
                        <li class="step_2"><span>2</span> Enter Bill to Details</li>
                        <li class="step_3"><span>3</span> Select Your Carriers</li>
                        <li class="step_4"><span>4</span> Setup Carrier Contract</li>
                        <li class="step_5"><span>5</span> Test My Rates</li>
                        <li class="step_6"><span>6</span> Setup Markup/Upcharge</li>
                    </ul>
                </div>
                <div class="reg_bot">
                    <h3>Fill Company Details</h3>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Your Company Name*</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Company Address Line 1</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Company Address Line 2</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Country*</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>City*</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>State*</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Postal*</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Contact Name</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Contact Email</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Contact Phone</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Fax</label>
                                <input type="text" class="form-control" placeholder="">
                            </div>
                            <label>Upload Company Logo</label>
                            <div class="file_upload">
                                <button type="button" class="btn btn-primary">Upload Logo</button>
                                <p>( Instruction about file size and image width and height )</p>
                            </div>
                            <label>Banner Message</label>
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="btn-cont">
                        <button  type="button" class="btn btn-primary">Continue Later</button>
                        <button  type="button" class="btn btn-primary">Next</button>
                    </div>
                </div>
          	</div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- /content section-->
  <!-- Page footer-->
  <footer class="footer-wrapper"> </footer>
  <!-- /Page footer-->
  </div>

  {!! Html::script( asset('js/jquery-1.10.2.js')) !!}
  {!! Html::script( asset('js/jquery.easing.1.3.js')) !!}
  {!! Html::script( asset('js/bootstrap.js')) !!}
  {!! Html::script( asset('js/jquery-ui.js')) !!}
  {!! Html::script( asset('js/jquery.multiselect.js')) !!}
  {!! Html::script( asset('js/easyResponsiveTabs.js')) !!}
  {!! Html::script( asset('js/footable.js')) !!}
  {!! Html::script( asset('js/ion.rangeSlider.js')) !!}
  {!! Html::script( asset('js/bootstrap-timepicker.min.js')) !!}
  {!! Html::script( asset('js/jquery.slimscroll.min.js')) !!}
  {!! Html::script( asset('js/bootstrap-tooltip.js')) !!}
  {!! Html::script( asset('js/common-ui.js')) !!}

  <!-- DRJ Feedback -->  
  {!! Html::style( asset('assets/styles/drj-feedback.css')) !!}
  {!! Html::script( asset('js/drj-feedback.js')) !!}

  <script>
    $(document).ready(function () {
      $.ajaxSetup({
         headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
      });
    });
    function submit() {
      var formData = new FormData($("#myForm")[0]);
      console.log(formData);
      var pathname = window.location.pathname;
      var url = "{{ action('IssueController@store') }}";
      $.ajax({
        type: "POST",
        url: url,
        data: formData,
        contentType : false,
        processData: false,
        cache: false,
        statusCode:{
          200 : function (response) {
            alert('We will solve your issue quickly.\nYou can track the issue in Messages.');
            console.log(response);
          },
          404 : function (response) {
            alert('Sorry, ServerError 404');
          },
        }
      });
    }
  </script>

  </body>
</html>
