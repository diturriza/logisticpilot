<!DOCTYPE html>
<html>
<head>
	<title>Logistic Pilot | @yield('title')</title>
	{!! Html::style( asset('css/bootstrap.min.css')) !!}
    {!! Html::style( asset('css/flexslider.css')) !!}
    {!! Html::style( asset('css/jquery-ui.css')) !!}
    {!! Html::style( asset('css/ion.rangeSlider.css')) !!}
    {!! Html::style( asset('css/ion.rangeSlider.skinHTML5.css')) !!}
    {!! Html::style( asset('css/jquery.multiselect.css')) !!}
    {!! Html::style( asset('css/footable.core.css')) !!}
    {!! Html::style( asset('css/easy-responsive-tabs.css')) !!}
    {!! Html::style( asset('css/bootstrap-timepicker.min.css')) !!}
    {!! Html::style( asset('css/screen.css')) !!}
    {!! Html::style( asset('css/font-awesome.min.css')) !!}
</head>
<body>
	@include('template.header')
	<meta name="_token" content="{!! csrf_token() !!}"/>
  	<!-- content section-->
  	<div class="body-container contract-page" id="page-wrapper">
		<section class="content-wrapper full-width">
		    <div class="content-subcontainer">
		        <div class="row clearfix">
				@yield('content')
				</div>
			</div>
		</section>
	</div>
	@include('template.footer')  
	{!! Html::script( asset('js/jquery-1.10.2.js')) !!}
  	{!! Html::script( asset('js/jquery.easing.1.3.js')) !!}
  	{!! Html::script( asset('js/bootstrap.js')) !!}
  	{!! Html::script( asset('js/jquery-ui.js')) !!}
  	{!! Html::script( asset('js/jquery.multiselect.js')) !!}
  	{!! Html::script( asset('js/easyResponsiveTabs.js')) !!}
  	{!! Html::script( asset('js/footable.js')) !!}
  	{!! Html::script( asset('js/ion.rangeSlider.js')) !!}
  	{!! Html::script( asset('js/bootstrap-timepicker.min.js')) !!}
  	{!! Html::script( asset('js/jquery.slimscroll.min.js')) !!}
  	{!! Html::script( asset('js/bootstrap-tooltip.js')) !!}

  	<!-- DRJ Feedback -->  
  	{!! Html::style( asset('assets/styles/drj-feedback.css')) !!}
  	{!! Html::script( asset('js/drj-feedback.js')) !!}
  <script>

    function send_submit(){
        var formData = new FormData($("#myForm")[0]);
        console.log(formData);
        var pathname = window.location.pathname;
        var url = "{{ action('IssueController@store') }}";
        $.ajax({
          type: "POST",
          url: url,
          data: formData,
          contentType : false,
          processData: false,
          cache: false,
          statusCode:{
            200 : function (response) {
              alert('We will solve your issue quickly.\nYou can track the issue in Messages.');
              console.log(response);
            },
            404 : function (response) {
              alert('Sorry, ServerError 404');
            },
          }
      });
    }
    $(document).ready(function () {

      $.ajaxSetup({
         headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
      });

      $('body').delegate('#send_submit','click',function(){
        console.log("delegate");
        send_submit();
      });
    });

  </script>

  @yield('scripts')
</body>
</html>