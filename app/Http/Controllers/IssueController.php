<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Issue;
use Illuminate\Http\Request;

class IssueController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$count_issues = Issue::all()->count();
		$issues = Issue::paginate(10);

		$solved = $issues->filter(function ($issue) {
			if ($issue->admin_id) {
				return $issue;
			}
		});
		$count_solved = $solved->count();
		$count_nosolved = $count_issues - $count_solved;

		if ($request->input() != null) {
			if ($request->input('filter') == 'solved') {
				$issues = Issue::where('admin_id', '<>', '')->paginate(10);
			} else {
				if ($request->input('filter') == 'nosolved') {
					$issues = Issue::where('admin_id', '=', null)->paginate(10);
				}
			}
		}

		$data = array('issues' => $issues,
			'count_issues' => $count_issues,
			'count_solved' => $count_solved,
			'count_nosolved' => $count_nosolved);

		return view('issue.index')->with('data', $data);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function issue(Request $request) {
		$count_issues = Issue::all()->count();
		$issues = Issue::paginate(10);

		$solved = $issues->filter(function ($issue) {
			if ($issue->admin_id) {
				return $issue;
			}
		});
		$count_solved = $solved->count();
		$count_nosolved = $count_issues - $count_solved;

		if ($request->input() != null) {
			if ($request->input('filter') == 'solved') {
				$issues = Issue::where('admin_id', '<>', '')->paginate(10);
			} else {
				if ($request->input('filter') == 'nosolved') {
					$issues = Issue::where('admin_id', '=', null)->paginate(10);
				}
			}
		}

		$data = array('issues' => $issues,
			'count_issues' => $count_issues,
			'count_solved' => $count_solved,
			'count_nosolved' => $count_nosolved);

		return view('issue.issue')->with('data', $data);

	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//this should eb checked by a middleware
		if (!\Auth::check()) {
			return "No user logged";
		}

		$issue = new Issue();
		$fileName = 'uploads/issues/' . \Auth::user()->id . "-" . md5(time());
		if ($request->has('img_val')) {

			$file = $request->get('img_val');
			$issue->mediaType = "image";
			$filteredData = substr($file, strpos($file, ",") + 1);

			//Decode the string
			$unencodedData = base64_decode($filteredData);

			//Save the image
			$fileName .= '.png';
			file_put_contents($fileName, $unencodedData);

		} else if ($request->has('video_val')) {

			$file = $request->get('video_val');
			$issue->mediaType = "video";
			move_uploaded_file($fileName, $file);
		}

		$issue->issueTxt = $request->get('issue');
		$issue->mediaUrl = $fileName;
		$issue->user_id = \Auth::user()->id;

		if ($issue->save()) {
			return "Issue sent complete.";
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$issue = Issue::findOrFail($id);
		return view('issue.view', compact('issue'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$issue = Issue::findOrFail($id);

		$this->validate($request, [
			'admin_id' => 'required',
			'response' => 'required',
		]);

		$input = array_except($request->all(), ['_method', '_token']);

		$issue->admin_id = $input['admin_id'];
		$issue->response = $input['response'];

		$issue->save();
		// dd($issue);

		// Session::flash('flash_message', 'Task successfully added!');

		return view('issue.view', compact('issue'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	//See Updates and explanation at: https://github.com/tazotodua/useful-php-scripts/
	protected function get_remote_data($url, $post_paramtrs = false) {
		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		if ($post_paramtrs) {
			curl_setopt($c, CURLOPT_POST, TRUE);
			curl_setopt($c, CURLOPT_POSTFIELDS, "var1=bla&" . $post_paramtrs);
		}
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0");
		curl_setopt($c, CURLOPT_COOKIE, 'CookieName1=Value;');
		curl_setopt($c, CURLOPT_MAXREDIRS, 10);
		$follow_allowed = (ini_get('open_basedir') || ini_get('safe_mode')) ? false : true;
		if ($follow_allowed) {
			curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
		}
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 9);
		curl_setopt($c, CURLOPT_REFERER, $url);
		curl_setopt($c, CURLOPT_TIMEOUT, 60);
		curl_setopt($c, CURLOPT_AUTOREFERER, true);
		curl_setopt($c, CURLOPT_ENCODING, 'gzip,deflate');
		$data = curl_exec($c);
		$status = curl_getinfo($c);
		curl_close($c);
		preg_match('/(http(|s)):\/\/(.*?)\/(.*\/|)/si', $status['url'], $link);
		$data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/|\/)).*?)(\'|\")/si', '$1=$2' . $link[0] . '$3$4$5', $data);
		$data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/)).*?)(\'|\")/si', '$1=$2' . $link[1] . '://' . $link[3] . '$3$4$5', $data);
		if ($status['http_code'] == 200) {
			return $data;
		} elseif ($status['http_code'] == 301 || $status['http_code'] == 302) {
			if (!$follow_allowed) {
				if (!empty($status['redirect_url'])) {
					$redirURL = $status['redirect_url'];
				} else {
					preg_match('/href\=\"(.*?)\"/si', $data, $m);
					if (!empty($m[1])) {
						$redirURL = $m[1];
					}
				}
				if (!empty($redirURL)) {
					return call_user_func(__FUNCTION__, $redirURL, $post_paramtrs);
				}
			}
		}
		return "ERRORCODE22 with $url!!<br/>Last status codes<b/>:" . json_encode($status) . "<br/><br/>Last data got<br/>:$data";
	}
}
