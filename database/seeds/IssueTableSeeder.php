<?php

use Illuminate\Database\Seeder;

class IssueTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('issues')->insert([
			'user_id' => 2,
			'issueTxt' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...',
			'mediaUrl' => 'http://www.seeklogo.net/wp-content/uploads/2012/12/real-madrid-c.f.-logo-vector.png',
			'mediaType' => 'image',
		]);
		DB::table('issues')->insert([
			'user_id' => 1,
			'issueTxt' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...',
			'mediaUrl' => 'http://www.seeklogo.net/wp-content/uploads/2012/12/real-madrid-c.f.-logo-vector.png',
			'mediaType' => 'image',
		]);
		DB::table('issues')->insert([
			'user_id' => 2,
			'issueTxt' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...',
			'mediaUrl' => 'http://www.seeklogo.net/wp-content/uploads/2012/12/real-madrid-c.f.-logo-vector.png',
			'mediaType' => 'image',
		]);
		DB::table('issues')->insert([
			'user_id' => 1,
			'issueTxt' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...',
			'mediaUrl' => 'http://www.seeklogo.net/wp-content/uploads/2012/12/real-madrid-c.f.-logo-vector.png',
			'mediaType' => 'image',
		]);
		DB::table('issues')->insert([
			'user_id' => 2,
			'issueTxt' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...',
			'mediaUrl' => 'http://www.seeklogo.net/wp-content/uploads/2012/12/real-madrid-c.f.-logo-vector.png',
			'mediaType' => 'image',
		]);
		DB::table('issues')->insert([
			'user_id' => 1,
			'issueTxt' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...',
			'mediaUrl' => 'http://www.seeklogo.net/wp-content/uploads/2012/12/real-madrid-c.f.-logo-vector.png',
			'mediaType' => 'image',
		]);
	}
}
